> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis4368 - Advanced Web Application Development

## DeVon Singleton

### Assignment 3 Requirements:

*Course Work Link*

 
    
    
    
    1. Entity Relationship Diagram
    
    2. Include data (at least 10 records in each table)
    
    3. Provide bitbucket read-only access to repo


#### README.md file should include the following items:

* Screenshot of AMPPS running http://localhost*
* Screenshot of running Servlet
* Screenshot of database connection
* 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:


*Screenshot of ERD*: 

![ERD Screenshot](img/a3.png)





*A3 MWB:*
[mwb](docs/a3.mwb)


*A3 SQL:*
[sql](docs/a3.sql)


####  Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link]
(https://bitbucket.org/Drs14c/bitbucketstationslocations/src/master/     "Bitbucket Station Locations")



*Local Web App Link:*
[Local Web App Link](http://localhost:9999/lis4368/a2/index.jsp "Web App")