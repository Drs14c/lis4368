> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis4368 - Advanced Web Application Development

## DeVon Singleton

### Assignment 5 Requirements:




* Check Server side validation	

* Prepared Statements - to prevent SQL Injection

* JSTL to prevent XSS
			 	
* Add insert functionality.  

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>



				
					   
			 	
			   			
								   
								   
							   
								   	
								   




#### Assignment Screenshots:

Screenshot of Form fill							|Screenshot of Thanks							|Screenshot of updated data	|
:----------------------------------------------------------------------:|:---------------------------------------------------------------------:|-------------------------------|
 *Screenshot of Form Fill*:![Screenshot](img/1.png)			|*Screenshot of Thanks*:![Screenshot](img/2.png)		 	| *Screenshot of added record*:![Screenshot](img/3.png)			









####  Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link]
(https://bitbucket.org/Drs14c/bitbucketstationslocations/src/master/     "Bitbucket Station Locations")

