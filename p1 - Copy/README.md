> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis4368 - Advanced Web Application Development

## DeVon Singleton

### Project 1 Requirements:


#### README.md file should include the following items:

* 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


### Project 1 requirements	|#### Assignment Screenshots:      |
-------------------------	|--------------------------------  |
Check client side validation	|*Screenshot of HomeScreen*:       |
			 	|				   |
Provide BitBucket read only 	|![Screenshot](img/home.png)       |
			 	|    				   |
Add fields for input to webpage	|*Screenshot of Failed Validation*:| 
				|				   |	
Use regexp to limit input	|![Screenshot](img/1.png)	   |
				|				   |
				|*Screenshot of Passed Validation*:|
				|				   |	
				|![Screenshot](img/2.png)          |




####  Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link]
(https://bitbucket.org/Drs14c/bitbucketstationslocations/src/master/     "Bitbucket Station Locations")

