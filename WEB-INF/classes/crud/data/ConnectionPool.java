package crud.data;

import java.sql.*;
import javax.sql.DataSource;
import javax.naming.InitialContext;
import javax.naming.NamingException;

//class
public class ConnectionPool{

    //static= object dont have to be initialized
    private static ConnectionPool pool = null;
    private static DataSource dataSource = null;
    
    //first constructor
    private ConnectionPool()
    {
        try{
            InitialContext ic = new InitialContext();
            dataSource = (DataSource) ic.lookup("java:/comp/env/jdbc/drs14c");
        }
        catch(NamingException e)
        {
            System.out.println(e);
        }
    }

    //thread safe user interaction = synchronized
    //2nd constructor with method 
public static synchronized ConnectionPool getInstance()
{
    if (pool==null)
    {
        pool = new ConnectionPool();
    }
    return pool;
    }
    
    //3rd constructor with method
public Connection getConnection()
{
    try{
        return dataSource.getConnection();
    }
    catch(SQLException e)
    {   System.out.println(e);
        return null;
}
}

public void freeConnection(Connection c)
{
    try{
        c.close();
    }
    catch (SQLException e)
    {   System.out.println(e);
    }
}

}