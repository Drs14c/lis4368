> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis4368 - Advanced Web Application Development

## DeVon Singleton

### Project 2 Requirements:




* Check Server side validation	

* Prepared Statements - to prevent SQL Injection

* JSTL to prevent XSS
			 	
* Add insert functionality.  

* Add update functionality

* Add delete functionality

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>



				
					   
			 	
			   			
								   
								   
							   
								   	
								   




#### Assignment Screenshots:

Screenshot of 	add							|Screenshot of display							|Screenshot of update					|Screenshot of delete								
:----------------------------------------------------------------------:|:---------------------------------------------------------------------:|-------------------------------------------------------|-----------------------------------------------------------------|
 *Screenshot of Form add*:![Screenshot](img/1.png)			|*Screenshot of display*:![Screenshot](img/2.png)		 	| *Screenshot of update record*:![Screenshot](img/3.png)|*Screenshot of deleted record*:![Screenshot](img/4.png)			









####  Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link]
(https://bitbucket.org/Drs14c/bitbucketstationslocations/src/master/     "Bitbucket Station Locations")

