> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below-- otherwise, points **will** be deducted.
>

# Lis4368 Advance Web Application Development

## DeVon Singleton


*Course Work Link*






1. [A1 README.md](a1/README.md)

	- Install AMPPS
	
	- Install JDK

	- Install TOMCAT

	- Create helloWorld.java

	- Create a Bitbucket repo

	- Provide git command descriptions

2.  [A2 README.md](a2/README.md)
	
	- My-SQL installation	

	- Create a Online Bookshop

		- (Create an automatic query that activate once an author is selected.. )

	- Create a hello world with servlet

	- Servlet compilation and usage

3. [A3 README.md](a3/README.md)
	
	- Create Entity Relationship Diagram

	- Include data (at least 10 records) 

4. [A4 README.md](a4/README.md)

	- Basic Server Side Validation

	- Business Servlet compilation




5. [A5 README.md](a5/README.md)
	
	- Prepared Statements

	- Jstl to prevent XSS

	- Add insert functionality 

6. [P1 README.md](p1/README.md)

	- Basic Client side validations using Regular Expression

7. [P2 README.md](p2/README.md)

	-  MVC Framework

	- Enable CRUD functionality

	
