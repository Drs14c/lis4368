> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis4368 - Advanced Web Application Development

## DeVon Singleton

### Assignment 2 Requirements:

*Course Work Link*

 
    
    
    
    1. Develop servelet app
    
    2. Questions 
    
    3. Provide screenshots of Servlet 


#### README.md file should include the following items:

* Screenshot of AMPPS running http://localhost*
* Screenshot of running Servlet
* Screenshot of database connection
* 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:


*Screenshot of directory*: 

![Directory Screenshot](img/directory.png)

[Hello](http://localhost:9999/hello "Web App")


*Screenshot of My home*: 
![My Home Screenshot](img/myhome.png)

[Hello Home](http://localhost:9999/hello/HelloHome.html "Web App")

*Screenshot of Say Hello*:

![Say Hello Screenshot](img/sayhello.png)

*Say Hello Link:*
[say hello](http://localhost:9999/hello/sayhello "Web App")


*Screenshot of Say hi*:
![Say Hi Screenshot](img/sayhi.png)

*Say Hi Link:*
[Local Web App Link](http://localhost:9999/hello/sayhi "Web App")


*Screenshot of bookshop*:

![Bookshop Screenshot](img/bookshop.png)

*QueryBook:*
[Local Web App Link](http://localhost:9999/hello/querybook.html "Web App")

*Screenshot of Book Query*:
![bookquery Screenshot](img/bookquery.png)


*Query:*
[Local Web App Link](http://localhost:9999/hello/query "Web App")


####  Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link]
(https://bitbucket.org/Drs14c/bitbucketstationslocations/src/master/     "Bitbucket Station Locations")



*Local Web App Link:*
[Local Web App Link](http://localhost:9999/lis4368/a2/index.jsp "Web App")