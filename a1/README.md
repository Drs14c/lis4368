> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis4368 - Advanced Web Application Development

## DeVon Singleton

### Assignment 1 Requirements:

*Course Work Link*

 
    
    1. Distributed Version Control with Git and Bitbucket	
    
    2. Enviroment Installations
    
    3. Questions 
    
    4. Provide screenshots of installation.



#### README.md file should include the following items:

* Screenshot of AMPPS running http://localhost*
* Screenshot of running java Hello
* Link to local lis4368 web app
* Git command with a short description


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - create a new local repository
2. git status - list the files you have changed and those you still need to add or commit
3. git add - add one or more files to staging(index)
4. git commit - commit changes to head (but not yet the remote repoistory) 
5. git push - send changes to the master branch of your remote repository
6. git pull - fetch and merged changes on the remote server to your working directory
7. git diff - view all the merge conflicts 

#### Assignment Screenshots:


*Screenshot of connection to http://localhost*:

![Tomcat Screenshot](img/tomcat.png)


*Screenshot of running Hello World java*:
![Hello World Screenshot](img/helloworld.png)




####  Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link]
(https://bitbucket.org/Drs14c/bitbucketstationslocations/src/master/     "Bitbucket Station Locations")

*Local Web App Link:*
[Local Web App Link](http://localhost:9999/lis4368/a1/index.jsp "Web App")
